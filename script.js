const getData = () => {
	fetch('https://jsonplaceholder.typicode.com/users').then((result) => {
		return result.json();
	}).then((data) => {
		let users = '';
		data.forEach((user) => {
			users +=`
			<div>
				<h2>${user.name}</h2>
				<p>${user.email}</p>
				<p>${user.company.name}</p>
				<p>${user.phone}</p>
			</div>	
			`
		});
	  document.querySelector('#output').innerHTML = users
	})
	.catch((err) => {

	})
} 

const addUser = (e) => {
	e.preventDefault();
let name = document.querySelector('#name').value
let email = document.querySelector('#email').value
let company = document.querySelector('#company').value
let phone = document.querySelector('#phone').value

fetch('https://jsonplaceholder.typicode.com/users', {
		method: 'POST',
		headers: {
			'Accept': 'application/json, text/plain',
			'Content-Type': 'application/json'
		},
		body:JSON.stringify({name: name, email: email, company: company, phone:phone})
	})
	.then((response) => {
		return response.json()
	}).then((data) => {
		console.log(data)
	})
	
}

const btn1 = document.querySelector('#btn1')
	btn1.addEventListener('click', getData)

const addForm = document.querySelector('#addUser')
addForm.addEventListener('submit', addUser);